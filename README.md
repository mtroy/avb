# AVB - Availabity

Short script to periodically check product availability on any market website.
The tool will popup the updates using your native notification manager (dunst, osd, notifyd...ensure you've started one).

## install
basic...<br>
> npm install<br>
> node main.js

## config
- Add product properties to a json file (avb.json sample provided)
- Add site ruleset to the script based on jquery crawling dom context.
