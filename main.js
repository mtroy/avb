var Crawler = require("crawler");
const notifier = require("node-notifier");
const fs = require("fs");
const homedir = require("os").homedir();

var listPath = homedir+"/.avb.lst";
var timer = 10000; //12*3600*1000; // 12 hours
if(!fs.existsSync(listPath))
    fs.writeFile(listPath, "{}", (err) => { if (err) console.log(err); })

var Promise = require("bluebird");
var getExchangeRates = require("get-exchange-rates");
var rates = getExchangeRates();
var fx = require("money");


var q = new Crawler
({
    maxConnections : 20,
    // This will be called for each crawled page
    callback : function (error, res, done)
    {
        if(error) console.log(error);
        else var $ = res.$;
        done();
    }
});



//var availability = [];
var ASIN;

/*var ASIN = {};
ASIN["B01EO45SDG"] = {title: "Dewalt scie a onglet", rule:"amazon"};
ASIN["B00K30PU5C"] = {title: "Veste soudeur", rule:"amazon"};
ASIN["B000NJXZ46"] = {title: "Bombe depousierante", rule:"amazon"};
ASIN["B07BMV3X6N"] = {title: "Carte mere H370Mitx dual ethernet", rule:"amazon"};
ASIN["B00CB5URA0"] = {title: "Piece etabli Clarke CWTS1", rule:"amazon"};*/


var userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36" [-] (3890)';
var ruleset = 
{
    amazon: 
    {
        uri: "https://www.amazon.fr/gp/product/",
        cb: function(error, res, done)
        {
            if(error) console.log(error);
            else var $ = res.$;
            var id = $("#productDetails_detailBullets_sections1 tr:first-child td").text().trim();

            // ######## check avail type


            // avail from market
            /*if($("[data-action='show-all-offers-display']").length)
                var avb = "V\tMarket\t";*/

            // avail from prime
            if($("#availability .a-color-success").length)
            {
                var from = "MarketPlace";
                if(!$("#availability .a-color-success").children().length)
                {
                    var from = "Prime";
                    var avb = $("#availability").text().toLowerCase();    
                    avb = avb.trim().replace(/^\.+|\.$/g, '');
                    if(avb != "en stock")
                    {
                        // remaining
                        var m = avb.match(new RegExp("(\\d+)", "ig"));
                        avb = m[0].padStart(5, ' ')+"\tPrime\t";
                    }else // fullstock
                        avb = avb.replace("en stock", "10000\tPrime\t");
                }else
                    var avb = "?? ??\tMarket\t";

                notifier.notify
                ({
                    title: ASIN[id].title,
                    message: "Newly available on "+from+"\n("+ruleset.amazon.uri+id+")"
                });

            }else
            {
                // unavail
                if($("#availability .a-color-price").length)
                    var avb = "-- --\tNONEWS\t";

                if($("#availability .a-color-state").length)
                    var avb = "-- --\tSOLDOUT\t";            
            }

            //console.log(avb+" == "+ASIN[id].title+" ("+ruleset.amazon.uri+id+")");
            done();

        }.bind(ASIN),
    },
    // farnell, otelo, priceminister
};

function querying()
{
    ASIN = JSON.parse(fs.readFileSync(listPath, {encoding:"utf8", flag:'r'}));
    var _queries = [];

    for(var v in ASIN)
    {
        var r = ASIN[v].rule;
        _queries.push
        ({
         userAgent,
         callback: ruleset[r].cb,
         uri: ruleset[r].uri+v
        });
    }
    q.queue([_queries]);
}

setInterval(querying, timer);

// Queue just one URL, with default callback
//c.queue('http://www.amazon.com');

// Queue a list of URLs
//c.queue(['http://www.google.com/','http://www.yahoo.com']);

// Queue URLs with custom callbacks & parameters


fx.rates = { AUD: 1.5015,
  BGN: 1.9558,
  BRL: 3.6772,
  CAD: 1.4655,
  CHF: 1.1472,
  CNY: 7.8108,
  CZK: 25.837,
  DKK: 7.4424,
  GBP: 0.89153,
  HKD: 9.1701,
  HRK: 7.5055,
  HUF: 311.42,
  IDR: 15803,
  ILS: 4.1229,
  INR: 76.496,
  JPY: 132.05,
  KRW: 1337.6,
  MXN: 21.411,
  MYR: 4.9651,
  NOK: 9.35,
  NZD: 1.6413,
  PHP: 59.949,
  PLN: 4.3,
  RON: 4.575,
  RUB: 67.527,
  SEK: 9.5195,
  SGD: 1.5998,
  THB: 39.16,
  TRY: 4.1905,
  USD: 1.1742,
  ZAR: 16.002,
  BTC: 0.00027136084751419896 };
fx.base = "EUR";



	q.queue([{
	    html: '<p>Tst</p>'
	}]);
// });

q.on('drain',function()
{
    // For example, release a connection to database.
    //    db.end();// close connection to MySQL

/*	var s = {};
	Object.keys(tabloid).sort().map((arr,key)=>
	{
		s[arr] = tabloid[arr].sort((a,b) =>
		{
			if(parseFloat(a[1]) <= parseFloat(b[1]))
				return -1;
			return 1;
		});
	});
	console.log(s);*/
	// console.log(JSON.stringify(s, null, 1));
});
